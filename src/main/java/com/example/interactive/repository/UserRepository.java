package com.example.interactive.repository;

import com.example.Interactive.entity.User;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.math.BigInteger;

public interface UserRepository extends CrudRepository<User,Long> {

    @Query("SELECT u FROM User as u WHERE u.mail =:mail AND u.password=:password") // dove la mail è uguale a quella che mettiamo noi
    User log(String mail, String password);



    // query per fare update in angular
    /*@Query(value = "UPDATE interactive.users SET name=:name, surname=:surname, birthday=:birthday, phone_number=:phoneNumber, mail=:mail, city=:city WHERE id=:id", nativeQuery = true )
    @Modifying // permettono di effettuare modifiche sul db
    @Transactional
    void updateUser(Long id, String name, String surname, String birthday, BigInteger phoneNumber, String city, String mail);


   @Query(value = "SELECT user FROM User as user WHERE user.mail=:mail")
    User findByMail(String mail);*/
}
