package com.example.interactive.entity;

import com.example.Interactive.entity.User;


import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "posts")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    private String description;
    @NotNull
    private String img;


    // relazioniamo 1 a N user->Post

    @ManyToOne
    @JoinColumn
    private User userPostId; // questo parametro è uguale a quello che passo nella classe user



}
