package com.example.Interactive.entity;


import com.example.interactive.entity.Post;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id") // SERVE PER FAR ANDARE LA RELAZIONE NEL DB
public class User {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;
    @NotNull
    private String name;
    @NotNull
    private String surname;
    @NotNull
    private String birthday;
    @NotNull
    private BigInteger phoneNumber;
    @NotNull
    @Column(unique = true) // non possono esserci due mail uguali
    private String mail;
    private String city;
    @NotNull
    private String password;
    private String img;

    // relazioniamo 1 a N user->Post

    @OneToMany(mappedBy = "userPostId") // nome variabile, classe più importante a meno importante poi parametro che li lega
    private List<Post> userPostId; //foreign key post

}
