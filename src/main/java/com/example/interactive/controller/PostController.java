package com.example.interactive.controller;


import com.example.interactive.entity.Post;
import com.example.interactive.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins="*")
public class PostController {


    @Autowired
    PostRepository postRepository;

    @PostMapping("/api/addpost") // CRUD CREATE
    public Post addPost(@RequestBody Post newPost){
        return postRepository.save(newPost);
    }


}
