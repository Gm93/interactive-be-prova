package com.example.interactive.controller;

import com.example.Interactive.entity.User;

import com.example.interactive.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins="*")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/api/adduser") // CRUD CREATE
    public User addUser(@RequestBody User newUser){
        return userRepository.save(newUser);
    }


    @PostMapping("/api/login") // per il login
    public ResponseEntity<User> loginUser(@RequestBody User userLog) {
        User user = userRepository.log(userLog.getMail(), userLog.getPassword());
        return new ResponseEntity<>(user, HttpStatus.OK);
    }


   /* @PutMapping(path = "/api/edit")
    public void submitForm(@RequestBody User user) {
        userRepository.updateUser(user.getId(), user.getName(), user.getSurname(), user.getBirthday(), user.getPhoneNumber(), user.getMail(), user.getCity());
    }


    @GetMapping(path = "/api/postEdit/{mail}")
    public User postEdit(@PathVariable ("mail") String mail ){
        return userRepository.findByMail(mail);

    }*/


}
